const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];


let listDiv = document.getElementById("root");
let ul = document.createElement("ul");

books.forEach(addItemList);

function addItemList(item, index) {
    try {
      if (!item["author"]) {
        throw new Error(`Item ${index} does not have author!`);
      }
      if (!item["name"]) {
        throw new Error(`Item ${index} does not have name!`);
      }
      if (!item["price"]) {
        throw new Error(`Item ${index} does not have price!`);
      }

      //console.log(item);
      let { author, name, price } = item;
      let li = document.createElement("li");
      li.innerHTML = `Book name: "${name}", Author: ${author}, Price: ${price} UAH`;
      ul.appendChild(li);
    } catch (err) {
      console.error(err.message);
    }

}

listDiv.appendChild(ul); 